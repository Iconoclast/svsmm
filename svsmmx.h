/******************************************************************************\
* Project:  Software Vector-to-Scalar MMX Virtual API                          *
* Authors:  R. J. Swedlow                                                      *
* Release:  2013.12.28                                                         *
* License:  CC0 Public Domain Dedication                                       *
*                                                                              *
* To the extent possible under law, the author(s) have dedicated all copyright *
* and related and neighboring rights to this software to the public domain     *
* worldwide. This software is distributed without any warranty.                *
*                                                                              *
* You should have received a copy of the CC0 Public Domain Dedication along    *
* with this software.                                                          *
* If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.             *
\******************************************************************************/

#ifndef _SVSMMX_H_INCLUDED
#define _SVSMMX_H_INCLUDED

/*
 * mostly finished, with a few missing things:
 *     1.  Any arithmetic add/sub/mul instructions.
 *     2.  Any arithmetic or logical shift instructions (left and right).
 *     3.  The six vector unpack hi/lo general support instructions.
 */

/*
 * This project is NOT a virtual machine or emulator of any of Intel's
 * instruction set architectures, including those supporting MMX.
 *
 * This project IS a high-level API for being able to conduct MMX operations
 * in pure software, scalar algorithms, modeled by ANSI-standard C language.
 *
 * It is useful to apply:
 *     1.  As a means of directly porting MMX source code to non-Intel ISAs.
 *     2.  In projects where MMX hardware is not necessarily available.
 *     3.  By beginners who are learning about MMX procedural behaviors.
 *
 * If the target environment is 64-bit, MMX is not legitimately useful, and,
 * although this API would continue to serve, it would be senseless in the
 * long run to use this API over the SSE2 support native to x64 systems.
 */

#ifdef _MMINTRIN_H_INCLUDED
#error "MMX API should not be included with SVSMMX."
#endif

#if defined(_MSC_VER)
#define ALIGNED8    __declspec(align(8))
#elif defined(GNU_C)
#define ALIGNED8    __attribute__((aligned(8)))
#else
#define ALIGNED8
#endif

#if defined(_MSC_VER)
typedef __int64             INT64;
typedef unsigned __int64    UINT64;
typedef __int32             INT32;
typedef unsigned __int32    UINT32;
typedef __int16             INT16;
typedef unsigned __int16    UINT16;
#elif defined(_STDINT)
typedef int64_t             INT64;
typedef uint64_t            UINT64;
typedef int32_t             INT32;
typedef uint32_t            UINT32;
typedef int16_t             INT16;
typedef uint16_t            UINT16;
#else
typedef long                INT64; /* the only ANSI approximation */
typedef unsigned long       UINT64; /* C99 `long long` support in other APIs */
typedef int                 INT32; /* likely to fail on 16-bit targets */
typedef unsigned int        UINT32;
typedef short               INT16;
typedef unsigned short      UINT16;
#endif

typedef signed char         INT8;
typedef unsigned char       UINT8;

typedef union {
    INT64 si64;
    UINT64 su64;
    INT32 pi32[2];
    UINT32 pu32[2];
    INT16 pi16[4];
    UINT16 pu16[4];
    INT8 pi8[8];
    UINT8 pu8[8];
} __m64;

/*
 * logical MMX
 */
__m64 _mm_and_si64(__m64 m1, __m64 m2)
{
    __m64 ret_slot;

    ret_slot.si64 = m1.si64 & m2.si64;
    return (ret_slot);
}
__m64 _mm_andnot_si64(__m64 m1, __m64 m2)
{
    __m64 ret_slot;

    ret_slot.si64 = (~m1.si64) & m2.si64;
    return (ret_slot);
}
__m64 _mm_or_si64(__m64 m1, __m64 m2)
{
    __m64 ret_slot;

    ret_slot.si64 = m1.si64 | m2.si64;
    return (ret_slot);
}
__m64 _mm_xor_si64(__m64 m1, __m64 m2)
{
    __m64 ret_slot;

    ret_slot.si64 = m1.si64 ^ m2.si64;
    return (ret_slot);
}

/*
 * comparison MMX
 */
__m64 _mm_cmpeq_pi8(__m64 m1, __m64 m2)
{
    __m64 ret_slot = { 0 }; /* false "potentially uninitialized" warnings */
    register int i;

    for (i = 0; i < 8; i++)
        ret_slot.pi8[i] = (m1.pi8[i] == m2.pi8[i]) ? 0xFF : 0x00;
    return (ret_slot);
}
__m64 _mm_cmpeq_pi16(__m64 m1, __m64 m2)
{
    __m64 ret_slot = { 0 }; /* false "potentially uninitialized" warnings */
    register int i;

    for (i = 0; i < 4; i++)
        ret_slot.pi16[i] = (m1.pi16[i] == m2.pi16[i]) ? 0xFFFF : 0x0000;
    return (ret_slot);
}
__m64 _mm_cmpeq_pi32(__m64 m1, __m64 m2)
{
    __m64 ret_slot = { 0 }; /* false "potentially uninitialized" warnings */
    register int i;

    for (i = 0; i < 2; i++)
        ret_slot.pi32[i] = (m1.pi32[i] == m2.pi32[i]) ? 0xFFFFFFFF : 0x00000000;
    return (ret_slot);
}
__m64 _mm_cmpgt_pi8(__m64 m1, __m64 m2)
{
    __m64 ret_slot = { 0 }; /* false "potentially uninitialized" warnings */
    register int i;

    for (i = 0; i < 8; i++)
        ret_slot.pi8[i] = (m1.pi8[i] > m2.pi8[i]) ? 0xFF : 0x00;
    return (ret_slot);
}
__m64 _mm_cmpgt_pi16(__m64 m1, __m64 m2)
{
    __m64 ret_slot = { 0 }; /* false "potentially uninitialized" warnings */
    register int i;

    for (i = 0; i < 4; i++)
        ret_slot.pi16[i] = (m1.pi16[i] > m2.pi16[i]) ? 0xFFFF : 0x0000;
    return (ret_slot);
}
__m64 _mm_cmpgt_pi32(__m64 m1, __m64 m2)
{
    __m64 ret_slot = { 0 }; /* false "potentially uninitialized" warnings */
    register int i;

    for (i = 0; i < 2; i++)
        ret_slot.pi32[i] = (m1.pi32[i] > m2.pi32[i]) ? 0xFFFFFFFF : 0x00000000;
    return (ret_slot);
}

/*
 * MMX utilities (pseudo-instructions, not real op-codes)
 * Some compilers complain about these being defined without some prefix.
 */
__m64 safe_mm_setzero_si64(void)
{
    __m64 ret_slot;

    ret_slot.si64 = 0x0000000000000000;
    return _mm_xor_si64(ret_slot, ret_slot);
}
__m64 safe_mm_set_pi32(INT32 i1, INT32 i0)
{
    __m64 ret_slot;

    ret_slot.pi32[0] = i0;
    ret_slot.pi32[1] = i1;
    return (ret_slot);
}
__m64 safe_mm_set_pi16(INT16 s3, INT16 s2, INT16 s1, INT16 s0)
{
    __m64 ret_slot;

    ret_slot.pi16[0] = s0;
    ret_slot.pi16[1] = s1;
    ret_slot.pi16[2] = s2;
    ret_slot.pi16[3] = s3;
    return (ret_slot);
}
__m64 safe_mm_set_pi8(
    INT8 b7, INT8 b6, INT8 b5, INT8 b4, INT8 b3, INT8 b2, INT8 b1, INT8 b0)
{
    __m64 ret_slot;
	
    ret_slot.pi8[0] = b0;
    ret_slot.pi8[1] = b1;
    ret_slot.pi8[2] = b2;
    ret_slot.pi8[3] = b3;
    ret_slot.pi8[4] = b4;
    ret_slot.pi8[5] = b5;
    ret_slot.pi8[6] = b6;
    ret_slot.pi8[7] = b7;
    return (ret_slot);
}
__m64 safe_mm_set1_pi32(INT32 i)
{
    __m64 ret_slot;

    ret_slot.pi32[1] = ret_slot.pi32[0] = i;
    return (ret_slot);
}
__m64 safe_mm_set1_pi16(INT16 s)
{
    __m64 ret_slot;

    ret_slot.pi16[3] = ret_slot.pi16[2] = ret_slot.pi16[1] = ret_slot.pi16[0]
  = s;
    return (ret_slot);
}
__m64 safe_mm_set1_pi8(INT8 b)
{
    __m64 ret_slot;

    ret_slot.pi8[7] = ret_slot.pi8[6] = ret_slot.pi8[5] = ret_slot.pi8[4]
  = ret_slot.pi8[3] = ret_slot.pi8[2] = ret_slot.pi8[1] = ret_slot.pi8[0]
  = b;
    return (ret_slot);
}

/*
 * some more MMX pseudo-instructions, customarily invented in this file
 */
__m64 _mm_setfull_si64(void)
{
    __m64 ret_slot = { 0 }; /* fix useless warning about uninitialized data */

    ret_slot = _mm_cmpeq_pi32(ret_slot, ret_slot);
    return (ret_slot);
}
__m64 _mm_not_si64(__m64 m1)
{
    __m64 ret_slot;

    ret_slot = _mm_setfull_si64();
#if (0)
    m1 = _mm_andnot_si64(m1, ret_slot);
    return (m1); /* non-commutative */
#else
    ret_slot = _mm_xor_si64(ret_slot, m1);
    return (ret_slot); /* commutative, usually what compilers generate */
#endif
}
__m64 _mm_nor_si64(__m64 m1, __m64 m2)
{
    __m64 ret_slot;

    ret_slot = _mm_or_si64(m1, m2);
    return _mm_not_si64(ret_slot); /* standard MIPS operation */
}
__m64 _mm_shuffle_pi16(__m64 a, const int n)
{ /* Realistically, `n` must not just be `const` but also a fixed immediate. */
    __m64 ret_slot;

    ret_slot.pi16[0] = a.pi16[(n >> 0) & 3];
    ret_slot.pi16[1] = a.pi16[(n >> 2) & 3];
    ret_slot.pi16[2] = a.pi16[(n >> 4) & 3];
    ret_slot.pi16[3] = a.pi16[(n >> 6) & 3];
    return (ret_slot);
}

/*
 * general support MMX
 */
void _mm_empty(void)
{
/*
 * Since we do not emulate the MMX hardware to this degree of hardware
 * accuracy, there is no need for this function in the absence of room
 * for conflict between the real MMX hardware's MM register file and
 * the standard x87 floating-point registers file.
 */
    return;
}
__m64 _mm_cvtsi32_si64(INT32 i)
{
    __m64 ret_slot;

    ret_slot.si64 = (INT64)(i);
    return (ret_slot);
}
INT32 _mm_cvtsi64_si32(__m64 m)
{
    INT32 ret_slot;

    ret_slot = (INT32)(m.si64);
    return (ret_slot);
}
/*
 * Before delving even further, it is necessary to organize a few algorithms
 * revolving around "saturated arithmetic"--that is, signed clamping and
 * unsigned clamping.  The general formula:
 *     f(x) = (x < MIN) ? MIN : ((x > MAX) ? MAX : x)
 * ... is sufficient for implementing these special SIMD operations, but
 * not really accurate.  The formula above is, of course, more easy to follow
 * and understand on the human level, but it risks several flaws in scalar
 * approaches such as branch weighing and misprediction.
 *
 * The Intel manuals do not really document what exactly these saturation
 * functions should look like on the low level of Intel hardware, but the
 * creator of this file has endeavored to staticize them into equally
 * concentrated, condition-independent algorithms in these defined methods:
 */
static INT8 ROM_sxsat8[2][2] = {
     0x00,-0x80,
    +0x7F,/* unreachable, ~0x00 ? */
};
INT8 SaturateSignedWordToSignedByte(INT16 w)
{
    ROM_sxsat8[0][0] &= 0x00;
    ROM_sxsat8[0][0] ^= (INT8)(w);
    return ROM_sxsat8[!!(-128+w & 0x80)][!!(+127-w & 0x80)];
}
static INT16 ROM_sxsat16[2][2] = {
     0x0000,-0x8000,
    +0x7FFF,/* unreachable, ~0x0000 ? */
};
INT16 SaturateSignedDoublewordToSignedWord(INT32 d)
{
    ROM_sxsat16[0][0] &= 0x0000;
    ROM_sxsat16[0][0] ^= (INT16)(d);
    return ROM_sxsat16[!!(-32768+d & 0x8000)][!!(+32767-d & 0x8000)];
}
static UINT8 ROM_zxsat8[2][2] = {
     0x00,-0x00,
    +0xFF,/* unreachable, ~0x00 ? */
};
UINT8 SaturateSignedWordToUnsignedByte(INT16 w)
{
    ROM_zxsat8[0][0] &= 0x00;
    ROM_zxsat8[0][0] ^= (UINT8)(w);
    return ROM_zxsat8[!!(-128+w & 0x80)][!!(+127-w & 0x80)];
}
__m64 _mm_packs_pi16(__m64 m1, __m64 m2)
{
    __m64 ret_slot;

    ret_slot.pi8[0] = SaturateSignedWordToSignedByte(m1.pi16[0]);
    ret_slot.pi8[1] = SaturateSignedWordToSignedByte(m1.pi16[1]);
    ret_slot.pi8[2] = SaturateSignedWordToSignedByte(m1.pi16[2]);
    ret_slot.pi8[3] = SaturateSignedWordToSignedByte(m1.pi16[3]);
    ret_slot.pi8[4] = SaturateSignedWordToSignedByte(m2.pi16[0]);
    ret_slot.pi8[5] = SaturateSignedWordToSignedByte(m2.pi16[1]);
    ret_slot.pi8[6] = SaturateSignedWordToSignedByte(m2.pi16[2]);
    ret_slot.pi8[7] = SaturateSignedWordToSignedByte(m2.pi16[3]);
    return (ret_slot);
}
__m64 _mm_packs_pi32(__m64 m1, __m64 m2)
{
    __m64 ret_slot;

    ret_slot.pi16[0] = SaturateSignedDoublewordToSignedWord(m1.pi32[0]);
    ret_slot.pi16[1] = SaturateSignedDoublewordToSignedWord(m1.pi32[1]);
    ret_slot.pi16[2] = SaturateSignedDoublewordToSignedWord(m2.pi32[0]);
    ret_slot.pi16[3] = SaturateSignedDoublewordToSignedWord(m2.pi32[1]);
    return (ret_slot);
}
__m64 _mm_packs_pu16(__m64 m1, __m64 m2)
{
    __m64 ret_slot;

    ret_slot.pu8[0] = SaturateSignedWordToUnsignedByte(m1.pi16[0]);
    ret_slot.pu8[1] = SaturateSignedWordToUnsignedByte(m1.pi16[1]);
    ret_slot.pu8[2] = SaturateSignedWordToUnsignedByte(m1.pi16[2]);
    ret_slot.pu8[3] = SaturateSignedWordToUnsignedByte(m1.pi16[3]);
    ret_slot.pu8[4] = SaturateSignedWordToUnsignedByte(m2.pi16[0]);
    ret_slot.pu8[5] = SaturateSignedWordToUnsignedByte(m2.pi16[1]);
    ret_slot.pu8[6] = SaturateSignedWordToUnsignedByte(m2.pi16[2]);
    ret_slot.pu8[7] = SaturateSignedWordToUnsignedByte(m2.pi16[3]);
    return (ret_slot);
}

#endif

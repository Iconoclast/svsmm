Currently this is an experiment.
Not quite as much to say right now.

I don't *know* with 100% certainty that a software version of the MMX hardware
intrinsics interface can be made in a fashion that it will be able to always,
one-on-one, substitute itself in for any of the native intrinsic method calls.

However, so far, it does seem to work fairly well.
The only exception known to date seems to be the Visual Studio compiler.
If using the built-in composite MMX intrinsics, Microsoft CL.EXE won't allow
these functions to be defined (not just re-defined--defined at all).

Once I can get the MMX virtualization header complete, I will probably try
to write one for SSE1 or SSE2, maybe the AMD 3dnow! extensions to learn about
them, and base them off of previously defined multimedia extensions, traced as
far back as MMX.  For example, I wrote a couple of SSE2 functions designed to
replicate SSSE3 or SSE4 intrinsics not available natively on my machine, but
SSE2 is many more steps complicated than MMX is, if not merely similar.

MMX stands for an arguable variety of things, as Intel would only acknowledge,
if anything, the lack of any official meaning for "MMX" as an abbreviation.
The Microsoft Developer Network (MSDN), however, indicates through its
documentation of the EMMS instruction in MMX, that the "MM" in "MMX" should
almost certainly mean "multimedia".  Other sources validate that it could mean
"matrix math" (or maybe even "multimedia math").  Literally speaking, MMX, for
those who don't know, is a (now very-old) extension to the native Intel x86
(more like, x87) instruction set architecture (ISA).  Because the presence of
its support relies only on the x87 floating-point unit and its FP registers
(coinciding, in fact, as the FP registers of the x87, but used as MMX "MM"
registers in a separate mode), there are no operating system requirements for
one to have support for MMX, aside from a 32-bit operating system (even Win95).

The same cannot be said for the various Intel Streaming SIMD Extensions (SSE)
extensions, which correct several limitations of using MMX by means of its own
register file (XMM0 to XMM7 or even XMM15, not MMX's MM0 to MM7) and thus have
certain things to pick over as far as which operating systems will comply.

With all that being said, MMX is way too old to be considered something that
would not be compatible on any of today's Intel-architecture machines.  (Even
if the limitation is having a 64-bit system, it is easily enough converted to
at least SSE2.)  However, this API is still useful for quickly porting code
which depends on the MMX functions to even the non-Intel systems using ANSI C.

Furthermore, as uninteresting as MMX support is nowadays, the virtual MMX layer
header should ideally serve as a basic building block for writing more virtual
layers of extensions based on IT, such as those for SSE1, SSE2 and maybe even
beyond if other people want to contribute.  As such, right now, it's nothing
very useful, but when I have time I will try to complete it to be different.

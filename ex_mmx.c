/*
 * demonstrations of the MMX instruction set
 * Currently not very complete, since I have yet to implement all op-codes.
 */

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#if (0)
#include <mmintrin.h>
typedef int                 INT32;
#define ALIGNED8    __declspec(align(8))
#else
#include "svsmmx.h"
#endif

int main(void)
{
    ALIGNED8 INT32 v0[2];
    __m64 test, tmp;

    v0[0] = 0xAA55;
    test = _mm_cvtsi32_si64(v0[0]);
#ifdef _MMINTRIN_H_INCLUDED
    tmp = _mm_setzero_si64();
#else
    tmp = safe_mm_setzero_si64();
#endif
    tmp = _mm_cmpeq_pi32(tmp, tmp);
    test = _mm_andnot_si64(test, tmp); /* test = ~test & ~0 */
    tmp = _mm_xor_si64(tmp, test);

    v0[0] = _mm_cvtsi64_si32(test);
    v0[1] = _mm_cvtsi64_si32(tmp);
    printf("test[31..0] = 0x%08X\ntmp [31..0] = 0x%08X\n", v0[0], v0[1]);
    _mm_empty();

    printf("Press ENTER to return.\n");
    fgetc(stdin);
    return 0;
}

/*
 * results when including "svsmmx.h" (my virtual layer API):
 * test[31..0] = 0xFFFF55AA
 * tmp [31..0] = 0x0000AA55
 *
 * results when including <mmintrin.h> (the real MMX hardware intrinsics):
 * test[31..0] = 0xFFFF55AA
 * tmp [31..0] = 0x0000AA55
 */

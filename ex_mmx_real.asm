; Listing generated by Microsoft (R) Optimizing Compiler Version 16.00.40219.01 

	.686P
	.XMM
	include listing.inc
	.model	flat

INCLUDELIB MSVCRT
INCLUDELIB OLDNAMES

PUBLIC	??_C@_0BI@BOEAOBPH@Press?5ENTER?5to?5return?4?6?$AA@ ; `string'
PUBLIC	??_C@_0CL@IEDOOJCH@test?$FL31?4?40?$FN?5?$DN?50x?$CF08X?6tmp?5?$FL31?4?40?$FN@ ; `string'
PUBLIC	_main
EXTRN	__imp__fgetc:PROC
EXTRN	__imp____iob_func:PROC
EXTRN	__imp__printf:PROC
;	COMDAT ??_C@_0BI@BOEAOBPH@Press?5ENTER?5to?5return?4?6?$AA@
CONST	SEGMENT
??_C@_0BI@BOEAOBPH@Press?5ENTER?5to?5return?4?6?$AA@ DB 'Press ENTER to r'
	DB	'eturn.', 0aH, 00H				; `string'
CONST	ENDS
;	COMDAT ??_C@_0CL@IEDOOJCH@test?$FL31?4?40?$FN?5?$DN?50x?$CF08X?6tmp?5?$FL31?4?40?$FN@
CONST	SEGMENT
??_C@_0CL@IEDOOJCH@test?$FL31?4?40?$FN?5?$DN?50x?$CF08X?6tmp?5?$FL31?4?40?$FN@ DB 't'
	DB	'est[31..0] = 0x%08X', 0aH, 'tmp [31..0] = 0x%08X', 0aH, 00H ; `string'
; Function compile flags: /Ogtpy
CONST	ENDS
_TEXT	SEGMENT
_main	PROC
; Line 22
	pxor	mm0, mm0
	mov	eax, 43605				; 0000aa55H
; Line 26
	pcmpeqd	mm0, mm0
	movd	mm1, eax
; Line 27
	pandn	mm1, mm0
	push	esi
; Line 32
	mov	esi, DWORD PTR __imp__printf
	movq	mm2, mm1
	pxor	mm0, mm1
	movd	ecx, mm0
	push	ecx
	movd	eax, mm2
	push	eax
	push	OFFSET ??_C@_0CL@IEDOOJCH@test?$FL31?4?40?$FN?5?$DN?50x?$CF08X?6tmp?5?$FL31?4?40?$FN@
	call	esi
; Line 33
	emms
; Line 35
	push	OFFSET ??_C@_0BI@BOEAOBPH@Press?5ENTER?5to?5return?4?6?$AA@
	call	esi
; Line 36
	call	DWORD PTR __imp____iob_func
	push	eax
	call	DWORD PTR __imp__fgetc
	add	esp, 20					; 00000014H
; Line 37
	xor	eax, eax
	pop	esi
; Line 38
	ret	0
_main	ENDP
_TEXT	ENDS
END
